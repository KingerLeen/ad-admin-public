# coding: utf-8
from django.conf.urls import url, include
from BLOG.views import *
from . import apis

urlpatterns = [
    url(r'^getAll$', apis.getAll),
    url(r'^postUser$', apis.postUser),
]