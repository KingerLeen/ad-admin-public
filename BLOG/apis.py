#!/usr/bin/env python
# encoding: utf-8

import datetime
import json
import base64
import jwt
import time
import uuid
from django import forms

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from .models import *
from . import views

@csrf_exempt
def getAll(request):
    '''
        拉取Advertisement信息
        GET
        返回：{
            "code": *
            "ret":{
                "massage": ***
            }
            "data":{
                "ad_123" (123为ad_id) :{
                    "ad_id": ***
                    ...所有信息(不包括创建时间和更新时间)...
                    图片返回的是文件地址，例如：media/contents/a.jpeg 
                    加上服务器地址即可直接访问该图片 ip:8000/media/contents/a.jpeg
                },
                //如果订单不止一个，按照上面的格式，一一返回
                    ... ...
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'GET':
        return JsonResponse(response)
    try:
        params =  request.GET

        response = {
            'code': 200,
            'ret': {'message': 'success'},
            "data":{

            }
        }

        l = WebInfo.objects.all()

        for obj in l:
            response["data"]["title"] = obj.title
            response["data"]["tel"] = obj.tel
            response["data"]["promise"] = obj.promise
            response["data"]["company"] = obj.company
            response["data"]["record"] = obj.record
            response["data"]["note"] = obj.note
            response["data"]["description"] = obj.description
            response["data"]["success"] = obj.success
            break
    except Exception, e:
        response = {'code': 404, 'ret': {'message': str(e)}}
    return JsonResponse(response)








@csrf_exempt
def postUser(request):
    ''' 
    修改用户信息
        POST

        传参：
        body:  (JSON)  (除token 其余均为可选参数)
        {
            "phone_number":
            "name":
            "loan_amount":
            "car_info":
            "city":

            "submit_url":
        }

        返回：{
            "code": *
            "ret":{
                "massage": ***success
            }
        }
    '''
    response = {'code': 405, 'ret': {'message': 'request error'}}
    if request.method != 'POST':
        return JsonResponse(response)
    try:
        params = json.loads(request.body)

        phone_number = params.get('phone_number','')
        name = params.get('name','')
        loan_amount = params.get('loan_amount','')
        car_info = params.get('car_info','')
        city = params.get('city','')
        submit_url = params.get('submit_url','')

        if(phone_number=="" or name=="" or loan_amount=="" or car_info=="" or city==""):
            return JsonResponse(response)
        Info.objects.create(
            phone_number=phone_number,
            name=name,
            loan_amount=loan_amount,
            car_info=car_info,
            city=city,
            submit_url=submit_url
        )
        response = {'code': 200, 'ret': {'message': 'success'}}
    except Exception, e:
        response = {'code': 404, 'ret': {'message': 'request error'}}
        response['ret'] = {'message': str(e)}
    return JsonResponse(response)

