# coding: utf-8
from __future__ import unicode_literals

from django.contrib.sites.models import Site
from django.db import models
from django.utils.datetime_safe import date, datetime

from DjangoUeditor.models import UEditorField

#***************************************************************************************

class Info(models.Model):
    '''
        数据表
    '''
    class Meta:
        verbose_name = u'用户表单'
        verbose_name_plural = verbose_name

    user_id = models.AutoField(u'用户号', primary_key=True)
    phone_number = models.CharField(u'电话号码', max_length=50, default="")
    name = models.CharField(u'姓名', max_length=50, default="")
    loan_amount = models.CharField(u'贷款金额', max_length=50, default="")
    car_info_choices = (
        (u"名下无车",u"名下无车"),
        (u"名下全款车",u"名下全款车"),
        (u"名下按揭车",u"名下按揭车"),
        (u"名下按揭还清",u"名下按揭还清")
    )
    car_info = models.CharField(u'车辆信息', max_length=50, blank=True, default="", choices=car_info_choices)
    city = models.CharField(u'城市',max_length=50, blank=True, default="")
    submit_time = models.DateTimeField(u'提交时间', auto_now_add=True)
    submit_url = models.URLField(u'提交地址', blank=True, default="")
    docking_personnel = models.CharField(u'对接人员',max_length=50, blank=True, default="")
    state_choices = (
        (u"未审核",u"未审核"),
        (u"已审核",u"已审核")
    )
    state = models.CharField(u'状态', max_length=50, blank=True, default="未审核", choices=state_choices)
    follow_remarks = models.TextField(u'跟进情况及其备注',max_length=500, blank=True, default="")

    def __unicode__(self):
        return self.phone_number




class WebInfo(models.Model):
    '''
        前端管理表
    '''
    class Meta:
        verbose_name = u'前端管理表'
        verbose_name_plural = verbose_name

    web_id = models.AutoField(u'web号', primary_key=True)

    title = models.CharField(u'网站标题', max_length=500, default="")
    tel = models.CharField(u'立即拨打', max_length=500, default="")
    promise = models.TextField(u'郑重承诺', max_length=500, default="")
    company = models.CharField(u'公司', max_length=500, default="")
    record = models.CharField(u'备案号', max_length=500, default="")
    note = models.CharField(u'下方注意栏', max_length=500, default="")
    description = models.TextField(u'网站描述(方便搜索引擎优先级)', max_length=500, default="")
    success = models.TextField(u'成功提示', max_length=500, default="")

    def __unicode__(self):
        return self.title