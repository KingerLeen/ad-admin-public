# coding: utf-8
from xadmin.views import ModelFormAdminView, CreateAdminView, UpdateAdminView, BaseAdminPlugin
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
import requests
from xadmin.views.edit import ModelFormAdminUtil
from django.apps import apps

class TemplateCreateSaveView(CreateAdminView):

    unzip_file = False

    def post_response(self):
        request = self.request
        requests.get()
        msg = _(
            'The %(name)s "%(obj)s" was added successfully.') % {'name': force_unicode(self.opts.verbose_name),
                                                            'obj': "<a class='alert-link' href='%s'>%s</a>" % (
                                                            self.model_admin_url('change', self.new_obj._get_pk_val()),
                                                            force_unicode(self.new_obj))}
        if "_continue" in request.POST:
            self.message_user(
                msg + ' ' + _("You may edit it again below."), 'success')
            return self.model_admin_url('change', self.new_obj._get_pk_val())
        if "_addanother" in request.POST:
            self.message_user(msg + ' ' + (_("You may add another %s below.") % force_unicode(self.opts.verbose_name)),
                              'success')
            return request.path
        else:
            self.message_user(msg, 'success')
            if "_redirect" in request.POST:
                return request.POST["_redirect"]
            elif self.has_view_permission():
                return self.model_admin_url('changelist')
            else:
                return self.get_admin_url('index')


class TemplateCreateSavePlugin(BaseAdminPlugin):
    # 传入的是field的name
    unzip_file = []

    def __init__(self, admin_view):
        super(TemplateCreateSavePlugin, self).__init__(admin_view)
        self.unzip_need_file = {}

    def init_request(self, *args, **kwargs):
        # 当设置unzip_file 的时候，active被设置为True
        active = bool(self.admin_view.has_change_permission() and self.unzip_file)
        if active:
            self.model_form = self.get_model_view(ModelFormAdminUtil, self.model).form_obj
        return active

    # 如何得到注册的字段和字段的值？
    # 应该先得到AdminView的方法么？
    def post_response(self, admin_url):
        pass


def getModelField(appname, modelname, exclude):
    modelobj = apps.get_model(appname, modelname)
    field = modelobj._meta.fields
    print(field)
