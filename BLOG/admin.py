# coding: utf-8
from xadmin.views import CommAdminView, ModelFormAdminView, ListAdminView, CreateAdminView, UpdateAdminView
from xadmin.layout import Main, Fieldset
from BLOG.models import *
import xadmin
from BLOG.plugins import ueditor
from django.utils.translation import ugettext as _
from django.utils.encoding import force_unicode
from django.conf import settings
import requests
import datetime

from exponent import send_push_message
from xadmin import views

class GlobalSetting(object):
    site_title = u"DKOnline后台管理系统"
    site_footer = u"518金融汽车平台"
    menu_style = "accordion"
    apps_label_title = {
        'auth': u'后台用户管理',
        'blog': u'数据库',
    }
class BaseSetting(object):
    enable_themes =True
    use_bootswatch =True
xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSetting)

class InfoAdmin(object):
    list_display = ('phone_number', 'name', 'loan_amount', 'car_info', 'city', 'submit_time', 'docking_personnel', 'state')
xadmin.site.register(Info, InfoAdmin)

class WebInfoAdmin(object):
    list_display = ('title', 'tel', 'company', 'record')
xadmin.site.register(WebInfo, WebInfoAdmin)
